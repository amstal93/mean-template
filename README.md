# MEAN Template

Use this template to create new web applications using a Dockerised MEAN stack.

## Using the template

Open your terminal, navigate to where you would like to create your project folder and run the follwing commands:

``` bash
wget https://gitlab.com/Kallahan23/mean-template/snippets/1983615/raw -O init.sh

sudo chmod +x init.sh

./init.sh
```

This will fetch the initialisation script, make it executable and run it.
Assuming the script finishes without any errors, it will self-destruct, leaving you only with your new project.
Open your new project with your favourite text editor and you're ready to go!

To run the application using Docker:

``` bash
cd your_project

docker-compose up --build
```

## What is a MEAN Stack

- MongoDB
- Express.js
- Angular
- Node.js

## External links

- <https://scotch.io/tutorials/create-a-mean-app-with-angular-2-and-docker-compose>
- <https://mherman.org/blog/dockerizing-an-angular-app>
