// modules ====================================================================
import { config as dotenvConfig } from 'dotenv';
dotenvConfig();

require('console-stamp')(console, {}); // TODO change require to import
import express from 'express';

// configuration ==============================================================
const app = express();

// HTTP port setting
const port = process.env.PORT || '3000';
app.set('port', port);

// define a route handler for the default home page
app.get('/', (req, res) => {
    res.send(`Hello world!! ${process.env.TEST}`);
});

// start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
